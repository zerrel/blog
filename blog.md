![alt text][scg] this image should be a lot smaller :)

# Zuul to Spring Cloud Gateway
With the growing popularity of Spring WebFlux as a solid, reactive alternative to traditional Spring Cloud applications, software solutions who depend on components such as Netflix OSS Zuul are forced to find reactive alternatives to those components. While Netflix has recently open-sourced their reactive implementation of Zuul–namely Zuul 2 (original, I know)–it might be worth considering other alternatives. One such alternative, Spring Cloud Gateway, stands out because it integrates tightly with the rest of the Spring ecosystem in typical use cases. This blog takes a peak at what goes into transitioning from Zuul to Spring Cloud Gateway.

## Implementation
As opposed to Zuul, Spring Cloud Gateway is **not** meant to be used as an embedded component. Rather, Spring Cloud Gateway will normally be implemented as a separate service. For anyone using Zuul as an embedded component planning on making the switch to Spring Cloud Gateway, this should therefore be taken under consideration.

### Zuul
The set-up for Zuul is quite straightforward. Assuming the proper dependencies have been defined, a custom filter can be defined which extends the ZuulFilter class, which can then be instantiated as a bean to be picked up by Spring at runtime. The figure below shows how the custom Zuul filter bean (RouteFilter) is instantiated in the proof-of-concept application. The RouteFilter.java class can be found in its corresponding git repository listed in appendix.

```java
@Bean
    public RouteFilter routeFilter() {
        return new RouteFilter();
    }
```
Except for adding the '@EnableZuulProxy' annotation to the main method of the application runner class (e.g. MyApplication.java), this is all that needs to be added in terms of Java code. Defining what to proxy and where to proxy it to, is done in the application.properties file, which requires two variables for the bare minimum routing functions: zuul.routes.app-name.path and zuul.ignored-patterns where 'app-name' is–you guessed it–the name of the service to which the incoming calls need to be redirected, and zuul.ignored-patterns takes a comma-separated list of paths. Requests directed at any of the paths in this list will be let through, all others will be redirected, hence the name ignored-patterns.

```properties
# Zuul
ribbon.euraka.enabled=false
zuul.routes.app-name.path=/**
zuul.routes.app-name.service-id=app-name
app-name.ribbon.listOfServers=http://localhost:8090
## Routes
zuul.ignored-patterns=\
  /project/search/id={id}, \
  /project/search/name={name}, \
  /project/edit/id={id}
```
Conversely, instead of telling Zuul what not to redirect, it is also possible to explicitly define which calls to redirect, letting the rest pass through. Deciding on which method to use depends mostly what the ratio between the calls to redirect and the calls to let pass through is. If the majority of calls are to be redirected, it is advised to define what not to redirect.

### Spring Cloud Gateway
While the syntax of the two implementations did differ substantially, the fundamental ideas behind both Zuul and Spring Cloud Gateway are nearly identical when it comes to defining criteria for what routes to redirect. For those implementing the migration, it should, therefore, be a relatively slight learning curve, assuming they are familiar with Netflix OSS components such as Zuul and Hystrix and filter configuration in general.

```java
@Bean
public RouteLocator customServerRouter(
    RouteLocatorBuilder builder) {
    return builder.routes()
            .route("generic_query_all", p -> p
                    .path("/**/search/all")
                    .filters(f -> f.setStatus(302))
                    .uri(baseUrl))
            .route("generic_create", p -> p
                    .path("/**/new")
                    .filters(f -> f.setStatus(302))
                    .uri(baseUrl))
            .build();
}
```

## Deviating from the typical use case
As to what happens when someone does attempt to embed Spring Cloud Gateway, it invites some peculiar behaviour. A good example of this is my experience with doing exactly that.
Having implemented Spring Cloud Gateway as and embedded component, running the application yielded unexpected results, as, even though Cloud Gateway was confirmed to be running, when requests came in, the application handled all requests, regardless of which request it was. After further testing, the problem turned out to be in the way Spring prioritizes beans. To shortly come back to Zuul, Zuul is implemented as a servlet and normally, when added to a project, is embedded into the Spring dispatch mechanism. This means any request passing through DispatcherServlet also passes through ZuulServlet and can, if needed, be redirected before getting handled by a controller. While Spring WebFlux has a similar handler for incoming requests, namely the DispatcherHandler, adding Spring Cloud Gateway to an application does not integrate into the DispatcherHandler the way Zuul is integrated into the DispatcherServlet. This effectively means embedding Spring Cloud Gateway into an application is not possible without potentially introducing some unwanted behaviour. Behaviour not unlike that which was observed when initially attempting to solve this issue.

When discovering the issue, the real root of the problem was not yet known. Instead, the suspected culprate was the order in which Spring would register beans, which was part of the problem. The hypothesis was that Spring would always give priority to controller beans when registering them at runtime. With this hypothesis, the most logical and simple solution was to simply remove the interfering endpoints from the application, which would essentially force the application–after determining there were no suitable endpoints–to redirect the incoming requests. The expected outcome was one of the following: either the request would be successfully redirected or the request would not come through at all. Making this change resulted in neither of the two; rather, the call was indeed routed to the simple HTTP server but was also duplicated in the process, meaning the request was made twice. The peculiar part is that the Spring Cloud Gateway component is essentially registered as a RequestHandler, so when an endpoint is not known to any registered controller, Spring will look for routes defined in any other registered RequestHandler. Spring does recognize the Cloud Gateway RequestHandler class as having a matching route, but for whatever inexplicable reason sends the request out twice.

When approaching one of the developers actively working on Spring Cloud Gateway about the issue, they were unable to explain this behaviour, and also pointed out that Cloud Gateway should not be implemented in this way either way. Instead, it should be implemented as a separate service which the clients send their requests to and are then directed.

[scg]: images/scg.png "Spring Cloud Gateway"